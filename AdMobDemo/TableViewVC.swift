//
//  ViewController.swift
//  AdMobDemo
//
//  Created by Sebastian on 21.02.2017.
//  Copyright © 2017 Sebastian. All rights reserved.
//

import UIKit
import GoogleMobileAds

class TableViewVC: UIViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.delegate = self
        tableView.dataSource = self
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 4
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! AdMobDemoCell
        
        // Configure the cell...
        if indexPath.row == 0 {
            cell.mainImageView.image = UIImage(named: "1")
            cell.titleLabel.text = "Red Lights, Lisbon"
            cell.authorLabel.text = "Monnathan Jak (@MonathanJak)"
            
        } else if indexPath.row == 1 {
            cell.mainImageView.image = UIImage(named: "2")
            cell.titleLabel.text = "Val Thorens, France"
            cell.authorLabel.text = "Monnathan Jak (@MonathanJak)"
            
        } else if indexPath.row == 2 {
            cell.mainImageView.image = UIImage(named: "3")
            cell.titleLabel.text = "Summer Beach Huts, England"
            cell.authorLabel.text = "Belive art (belive-art.com)"
            
        } else {
            cell.mainImageView.image = UIImage(named: "4")
            cell.titleLabel.text = "Taxis, NYC"
            cell.authorLabel.text = "Monnathan Jak (@MonathanJak)"
            
        }
        
        return cell
    }

    
    

}

