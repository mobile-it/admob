//
//  AdMobDemoCell.swift
//  AdMobDemo
//
//  Created by Sebastian on 22.02.2017.
//  Copyright © 2017 Sebastian. All rights reserved.
//

import UIKit

class AdMobDemoCell: UITableViewCell {

    @IBOutlet weak var mainImageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var authorLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
